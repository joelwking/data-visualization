#
#     Copyright (c) 2024 Joel W. King
#     All rights reserved.
#
#     author: @joelwking
#     written:  3 April 2024
#     references:
#      https://fabiorosado.dev/blog/install-conda-in-docker/ 
#
FROM python:3.10.14-slim-bullseye
LABEL maintainer="Joel W. King" email="programmable.networks@gmail.com"
RUN apt update && \
    apt -y install git && \
    apt -y install wget && \
    apt -y install python3-dev && \
    apt -y install build-essential && \
    pip3 install --upgrade pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
#
RUN mkdir /src
COPY requirements.txt /src
WORKDIR /src
RUN pip install -r requirements.txt
#
# Install miniconda
ENV CONDA_DIR /opt/conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda

# Put conda in path so we can use conda activate
ENV PATH=$CONDA_DIR/bin:$PATH

